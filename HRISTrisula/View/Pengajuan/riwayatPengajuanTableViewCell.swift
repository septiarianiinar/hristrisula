//
//  riwayatPengajuanTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 02/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class riwayatPengajuanTableViewCell: UITableViewCell {

    @IBOutlet weak var pengajuanContentView: UIView!
    @IBOutlet weak var pengajuanIndicatorView: UIView!
    @IBOutlet weak var pengajuanDateLabel: UILabel!
    @IBOutlet weak var pengajuanMonthLabel: UILabel!
    @IBOutlet weak var pengajuanYearLabel: UILabel!
    @IBOutlet weak var pengajuanCategoryLabel: UILabel!
    @IBOutlet weak var pengajuanTitleLabel: UILabel!
    @IBOutlet weak var pengajuanHargaLabel: UILabel!
    @IBOutlet weak var pengajuanStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        pengajuanContentView.layer.cornerRadius = 5.0
        pengajuanContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        pengajuanContentView.layer.borderWidth = 1.0
        pengajuanContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
