//
//  dinasTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class dinasTableViewCell: UITableViewCell {

    @IBOutlet weak var dinasContentView: UIView!
    @IBOutlet weak var dinasIndicatorView: UIView!
    @IBOutlet weak var dinasDateLabel: UILabel!
    @IBOutlet weak var dinasMonthLabel: UILabel!
    @IBOutlet weak var dinasYearLabel: UILabel!
    @IBOutlet weak var dinasTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        dinasContentView.layer.cornerRadius = 5.0
        dinasContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        dinasContentView.layer.borderWidth = 1.0
        dinasContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
