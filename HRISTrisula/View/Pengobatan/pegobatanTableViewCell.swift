//
//  pegobatanTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class pegobatanTableViewCell: UITableViewCell {

    @IBOutlet weak var pengobatanContentView: UIView!
    @IBOutlet weak var pengobatanIndicatorView: UIView!
    @IBOutlet weak var pengobatanDateLabel: UILabel!
    @IBOutlet weak var pengobatanMonthLabel: UILabel!
    @IBOutlet weak var pengobatanYearLabel: UILabel!
    @IBOutlet weak var pengobatanCategoryLabel: UILabel!
    @IBOutlet weak var pengobatanTitleLabel: UILabel!
    @IBOutlet weak var pengobatanHargaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        pengobatanContentView.layer.cornerRadius = 5.0
        pengobatanContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        pengobatanContentView.layer.borderWidth = 1.0
        pengobatanContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
