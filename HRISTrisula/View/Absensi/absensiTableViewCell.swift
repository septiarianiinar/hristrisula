//
//  absensiTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 30/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class absensiTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var masukTimeLabel: UILabel!
    @IBOutlet weak var keluarTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
