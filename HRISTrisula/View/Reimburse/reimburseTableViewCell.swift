//
//  reimburseTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class reimburseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reimburseContentView: UIView!
    @IBOutlet weak var reimburseIndicatorView: UIView!
    @IBOutlet weak var reimburseDateLabel: UILabel!
    @IBOutlet weak var reimburseMonthLabel: UILabel!
    @IBOutlet weak var reimburseYearLabel: UILabel!
    @IBOutlet weak var reimburseTitleLabel: UILabel!
    @IBOutlet weak var reimburseHargaLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        reimburseContentView.layer.cornerRadius = 5.0
        reimburseContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        reimburseContentView.layer.borderWidth = 1.0
        reimburseContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
