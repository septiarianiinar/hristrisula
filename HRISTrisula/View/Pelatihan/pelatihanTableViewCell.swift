//
//  pelatihanTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class pelatihanTableViewCell: UITableViewCell {

    @IBOutlet weak var pelatihanContentView: UIView!
    @IBOutlet weak var pelatihanIndicatorView: UIView!
    @IBOutlet weak var pelatihanDateLabel: UILabel!
    @IBOutlet weak var pelatihanMonthLabel: UILabel!
    @IBOutlet weak var pelatihanYearLabel: UILabel!
    @IBOutlet weak var pelatihanTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        pelatihanContentView.layer.cornerRadius = 5.0
        pelatihanContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        pelatihanContentView.layer.borderWidth = 1.0
        pelatihanContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
