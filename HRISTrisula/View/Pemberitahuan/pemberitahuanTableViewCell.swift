//
//  pemberitahuanTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 30/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class pemberitahuanTableViewCell: UITableViewCell {

    @IBOutlet weak var pemberitahuanContentView: UIView!
    @IBOutlet weak var pemberitahuanIndicatorView: UIView!
    @IBOutlet weak var pemberitahuanDateLabel: UILabel!
    @IBOutlet weak var pemberitahuanMonthLabel: UILabel!
    @IBOutlet weak var pemberitahuanYearLabel: UILabel!
    @IBOutlet weak var pemberitahuanCategoryLabel: UILabel!
    @IBOutlet weak var pemberitahuanTitleLabel: UILabel!
    @IBOutlet weak var pemberitahuanHargaLabel: UILabel!
    @IBOutlet weak var pemberitahuanStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        pemberitahuanContentView.layer.cornerRadius = 5.0
        pemberitahuanContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        pemberitahuanContentView.layer.borderWidth = 1.0
        pemberitahuanContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
