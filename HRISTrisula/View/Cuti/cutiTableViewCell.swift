//
//  cutiTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class cutiTableViewCell: UITableViewCell {

    @IBOutlet weak var cutiContentView: UIView!
    @IBOutlet weak var cutiIndicatorView: UIView!
    @IBOutlet weak var cutiDateLabel: UILabel!
    @IBOutlet weak var cutiMonthLabel: UILabel!
    @IBOutlet weak var cutiYearLabel: UILabel!
    @IBOutlet weak var cutiTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cutiContentView.layer.cornerRadius = 5.0
        cutiContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        cutiContentView.layer.borderWidth = 1.0
        cutiContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
