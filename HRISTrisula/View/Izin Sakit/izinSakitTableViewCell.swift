//
//  izinSakitTableViewCell.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class izinSakitTableViewCell: UITableViewCell {

    @IBOutlet weak var izinContentView: UIView!
    @IBOutlet weak var izinIndicatorView: UIView!
    @IBOutlet weak var izinDateLabel: UILabel!
    @IBOutlet weak var izinMonthLabel: UILabel!
    @IBOutlet weak var izinYearLabel: UILabel!
    @IBOutlet weak var izinTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        izinContentView.layer.cornerRadius = 5.0
        izinContentView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        izinContentView.layer.borderWidth = 1.0
        izinContentView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
