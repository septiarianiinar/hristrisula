//
//  customPickerView.swift
//  RefillMyBottle
//
//  Created by Codelabs on 7/11/18.
//  Copyright © 2018 Codelabs. All rights reserved.
//

import UIKit

@objc protocol pickerDelegate:class{
    @objc optional func picker(picker:UIPickerView, doneAt textfield:UITextField)
    func picker(textField:UITextField?, selectAt index:Int)
}

class customPickerView: UIView, UIPickerViewDelegate, UIPickerViewDataSource {

    
    @IBOutlet weak var mypickerView: UIPickerView!
    
    var textField : UITextField?
    var isDirectly = true
    var isSelectDefault = false
    weak var delegate:pickerDelegate?
    
    var datas : [Any] = []{
        didSet{
            if isSelectDefault{
                mypickerView.reloadAllComponents()
                if datas.count > 0{
                    pickerView(mypickerView, didSelectRow: 0, inComponent: 0)
                    delegate?.picker(textField: textField, selectAt: 0)
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // MARK: - Private Helper Methods
    
    // Performs the initial setup.
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        self.mypickerView.delegate = self
        self.mypickerView.dataSource = self
    }
    
    // Loads a XIB file into a view and returns this view.
    private func viewFromNibForClass() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        if let tf = textField {
            delegate?.picker?(picker: self.mypickerView, doneAt: tf)
            _ = tf.delegate?.textFieldShouldReturn?(tf)
            tf.resignFirstResponder()
        }
    }
}

extension customPickerView{
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //        return datas.count + 1
        return datas.count
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let textLabel = UILabel()
        textLabel.textAlignment = .center
        textLabel.adjustsFontSizeToFitWidth = true
        textLabel.backgroundColor = UIColor.clear
        
        //        textLabel.text = (row == 0) ? "Select" : String(describing: datas[row-1])
        //        textLabel.font = (row == 0) ? UIFont.systemFont(ofSize: 30.0) : UIFont.systemFont(ofSize: 18.0)
        //        textLabel.textColor = (row == 0) ? UIColor.lightGray : UIColor.black
        
        textLabel.text =  String(describing: datas[row])
        textLabel.font = UIFont(name: "Helvetica Neue", size: 17)
        textLabel.textColor = UIColor(red: 17/255, green: 35/255, blue: 70/255, alpha: 1)
        
        return textLabel
    }
    
    // MARK: - Delegate Method
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //        textField?.text = (row == 0) ? "" : String(describing: datas[row-1])
        //        delegate?.picker(textField: textField, selectAt: row-1)
        if isDirectly{
            if datas.count > 0{
                textField?.text = String(describing: datas[row])
                delegate?.picker(textField: textField, selectAt: row)
                textField?.sendActions(for: .editingChanged)
            }
        }
    }
    
}
