//
//  UITextfield+Extension.swift
//  karuizawa
//
//  Created by Khairul Rijal on 5/12/17.
//  Copyright © 2017 Khairul Rijal. All rights reserved.
//

import UIKit
import TextFieldEffects

public enum BaseUITextFieldInputType {
    case Birth, Date, Time, Gender, YearBirth, MonthYear, Custom
}

extension UITextField{
    
    //MARK: - Set Input Picker with Datas
    func setInputWith(arrayString data:[Any], delegate:pickerDelegate?, isDirectly:Bool = true, isSelectDefault:Bool = false){
        let picker = customPickerView()
        picker.textField = self
        picker.isDirectly = isDirectly
        picker.isSelectDefault = isSelectDefault
        picker.delegate = delegate
        picker.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        picker.datas = data
        inputView = picker
        
        guard rightView == nil else{
            return
        }
        
        let dropDown = UIButton(type: .custom)
        dropDown.frame =  CGRect(x: 0, y: 0, width: 30, height: 30)
//        dropDown.setImage(#imageLiteral(resourceName: "ic_arrow_drop_down"), for: .normal)
//        dropDown.tintColor = #colorLiteral(red: 0, green: 0.2223632336, blue: 0.3668234348, alpha: 1)
        dropDown.tintColor = UIColor(red: 17/255, green: 35/255, blue: 70/255, alpha: 1)
        dropDown.addTarget(self, action: #selector(dropDownTap(sender:)), for: .touchUpInside)
        
        if self is HoshiTextField{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 50))
            view.addSubview(dropDown)
        
            self.rightView = view
            self.rightViewMode = .always
        }else{
            self.rightView = dropDown
            self.rightViewMode = .always
        }
    }
    
    @objc
    func dropDownTap(sender:Any){
        if isFirstResponder{
            self.resignFirstResponder()
        }else{
            self.becomeFirstResponder()
        }
    }
    
    //MARK: - Set Input Date Picker
    func setDatePicker(mode:UIDatePicker.Mode = .date, locale:Locale? = nil, date:Date? = nil){
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneTap))
        toolbar.setItems([flex, doneButton], animated: false)
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = mode
        datePicker.locale = locale
        
        if let _date = date{
            datePicker.date = _date
        }
        
        self.inputView = datePicker
        self.inputAccessoryView = toolbar
    }
    
    @objc
    private func doneTap(){
        _ = self.delegate?.textFieldShouldReturn?(self)
    }
}
