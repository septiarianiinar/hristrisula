//
//  riwayatPengajuanViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 02/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class RiwayatPengajuanViewController: BaseViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var tipeView: UIView!
    @IBOutlet weak var tipeLabel: UILabel!
    @IBOutlet weak var tipeButton: UIButton!
    @IBOutlet weak var tipeTableView: UITableView!
    @IBOutlet var statusOuterView: [UIView]!
    @IBOutlet var statusInnerView: [UIView]!
    @IBOutlet var statusButton: [UIButton]!
    @IBOutlet weak var pengajuanTableView: UITableView!
    
    var selectedStatus:Int = 1
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        
        let backButtonSize:CGFloat = (self.navigationController?.navigationBar.frame.height ?? 0.0) - 5.0
        let backImage:UIImage = self.imageWithImage(image: (UIImage(named: "back_arrow") ?? nil)!, scaledToSize: CGSize(width: (backButtonSize - 10.0), height: (backButtonSize - 10.0)))
        let backBtn = UIButton()
        backBtn.addTarget(self, action: #selector(backTap(_:)), for: .touchUpInside)
        backBtn.setImage(backImage, for: .normal)
        backBtn.contentMode = .scaleAspectFit
        backBtn.frame = CGRect(x: 0, y: 0, width: backButtonSize, height: backButtonSize)
        let backButton = UIBarButtonItem.init()
        backButton.customView = backBtn
        
        self.navigationItem.leftBarButtonItems = [backButton]
        
        let titleLabel = UILabel()
        titleLabel.text = "RIWAYAT PENGAJUAN"
        titleLabel.font = UIFont(name: "Avenir-Black", size: 20.0)
        titleLabel.textColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1)
        let titleItem = UIBarButtonItem.init()
        titleItem.customView = titleLabel
        
        self.navigationItem.rightBarButtonItems = [titleItem]
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        //Set Tipe Dropdown
        tipeView.layer.borderWidth = 1.0
        tipeView.layer.borderColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1).cgColor
        tipeButton.addTarget(self, action: #selector(tipeButtonTap(_:)), for: .touchUpInside)
        
        //Set Status Category View
        for var i in (0..<statusOuterView.count) {
            statusOuterView[i].layer.cornerRadius = statusOuterView[i].frame.height / 2
            statusOuterView[i].layer.borderWidth = 1.0
            if statusOuterView[i].tag == 1 {
                statusOuterView[i].layer.borderColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1).cgColor
            }
            else if statusOuterView[i].tag == 2 {
                statusOuterView[i].layer.borderColor = UIColor(red: 30/255, green: 213/255, blue: 67/255, alpha: 1).cgColor
            }
            else if statusOuterView[i].tag == 3 {
                statusOuterView[i].layer.borderColor = UIColor(red: 252/255, green: 6/255, blue: 31/255, alpha: 1).cgColor
            }
            else if statusOuterView[i].tag == 4 {
                statusOuterView[i].layer.borderColor = UIColor(red: 252/255, green: 228/255, blue: 9/255, alpha: 1).cgColor
            }
        }
        
        for var i in(0..<statusInnerView.count) {
            statusInnerView[i].layer.cornerRadius = statusInnerView[i].frame.height / 2
        }
        
        //Set Table View
        pengajuanTableView.separatorStyle = .none
        pengajuanTableView.register(UINib.init(nibName: "riwayatPengajuanTableViewCell", bundle: nil), forCellReuseIdentifier: "pengajuanCell")
        
        //Set Button Handler
        homeButton.addTarget(self, action: #selector(homeBtnTap(_:)), for: .touchUpInside)
    }
    
    private func setDataView() {
        for var i in(0..<statusInnerView.count) {
            if let tag = statusInnerView[i].tag as? Int {
                if tag == selectedStatus {
                    if tag == 1 {
                        statusInnerView[i].backgroundColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1)
                    }
                    else if tag == 2 {
                        statusInnerView[i].backgroundColor = UIColor(red: 30/255, green: 213/255, blue: 67/255, alpha: 1)
                    }
                    else if tag == 3 {
                        statusInnerView[i].backgroundColor = UIColor(red: 252/255, green: 6/255, blue: 31/255, alpha: 1)
                    }
                    else if tag == 4 {
                        statusInnerView[i].backgroundColor = UIColor(red: 252/255, green: 228/255, blue: 9/255, alpha: 1)
                    }
                } else {
                    statusInnerView[i].backgroundColor = UIColor.white
                }
            }
        }
    }
    
    //MARK: - Set Data
    override func setData() {
        super.setData()
        
        setDataView()
    }
    
    //MARK: - Action Handler
    @objc private func backTap(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func homeBtnTap(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func tipeButtonTap(_ sender: UIButton) {
        Toast(text: "Soon").show()
    }
    
    @IBAction private func statusCategoryTap(_ sender: Any) {
        guard let button = sender as? UIButton, let tag = button.tag as? Int else {
            return
        }
        
        guard tag != selectedStatus else {
            return
        }
        
        selectedStatus = tag
        setDataView()
    }

}

extension RiwayatPengajuanViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == pengajuanTableView {
            return 3
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == pengajuanTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pengajuanCell", for: indexPath) as! riwayatPengajuanTableViewCell
            
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == pengajuanTableView {
            return 65.0
        }
        else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == pengajuanTableView {
            return 65.0
        }
        else {
            return 0.0
        }
    }
}
