//
//  IzinSakitViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 03/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class IzinSakitViewController: BaseViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var riwayatPengajuanButton: UIButton!
    
    @IBOutlet weak var tahunLabel: UILabel!
    @IBOutlet weak var izinTableView: UITableView!
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        
        let backButtonSize:CGFloat = (self.navigationController?.navigationBar.frame.height ?? 0.0) - 5.0
        let backImage:UIImage = self.imageWithImage(image: (UIImage(named: "back_arrow") ?? nil)!, scaledToSize: CGSize(width: (backButtonSize - 10.0), height: (backButtonSize - 10.0)))
        let backBtn = UIButton()
        backBtn.addTarget(self, action: #selector(backTap(_:)), for: .touchUpInside)
        backBtn.setImage(backImage, for: .normal)
        backBtn.contentMode = .scaleAspectFit
        backBtn.frame = CGRect(x: 0, y: 0, width: backButtonSize, height: backButtonSize)
        let backButton = UIBarButtonItem.init()
        backButton.customView = backBtn
        
        self.navigationItem.leftBarButtonItems = [backButton]
        
        let titleLabel = UILabel()
        titleLabel.text = "IZIN / SAKIT"
        titleLabel.font = UIFont(name: "Avenir-Black", size: 20.0)
        titleLabel.textColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1)
        let titleItem = UIBarButtonItem.init()
        titleItem.customView = titleLabel
        
        self.navigationItem.rightBarButtonItems = [titleItem]
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        //Set Table View
        izinTableView.separatorStyle = .none
        izinTableView.register(UINib.init(nibName: "izinSakitTableViewCell", bundle: nil), forCellReuseIdentifier: "daftarCell")
        
        //Set Button Handler
        homeButton.addTarget(self, action: #selector(homeBtnTap(_:)), for: .touchUpInside)
        addButton.addTarget(self, action: #selector(addTap(_:)), for: .touchUpInside)
        riwayatPengajuanButton.addTarget(self, action: #selector(riwayatPengajuanTap(_:)), for: .touchUpInside)
    }
    
    //MARK: - Refresh Data
    override func refreshData() {
        super.refreshData()
        
    }
    
    //MARK: - Action Handler
    @objc private func backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func homeBtnTap(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func addTap(_ sender: UIButton) {
        let tambahIzinVC = self.storyboard?.instantiateViewController(withIdentifier: "TambahIzinViewController") as! TambahIzinViewController
        
        if let viewControllers = self.navigationController?.viewControllers{
            if let index = viewControllers.index(of: self){
                var tmp = viewControllers
                tmp[index] = tambahIzinVC
                
                self.navigationController?.pushViewController(tambahIzinVC, animated: true)
            }
        }
    }
    
    @objc private func riwayatPengajuanTap(_ sender: UIButton) {
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let riwayatPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "RiwayatPengajuanViewController") as! RiwayatPengajuanViewController
        self.navigationController?.pushViewController(riwayatPengajuanVC, animated: true)
    }
    
}

extension IzinSakitViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "daftarCell", for: indexPath) as! izinSakitTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
