//
//  RegistrasiViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 29/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class RegistrasiViewController: BaseViewController {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var telpField: UITextField!
    @IBOutlet weak var namaField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        telpField.layer.cornerRadius = 16.0
        telpField.layer.borderColor = UIColor(red: 108/255, green: 99/255, blue: 255/255, alpha: 0.36).cgColor
        telpField.layer.borderWidth = 1.0
        namaField.layer.cornerRadius = 16.0
        namaField.layer.borderColor = UIColor(red: 108/255, green: 99/255, blue: 255/255, alpha: 0.36).cgColor
        namaField.layer.borderWidth = 1.0
        
        shadowView.addShadow(shadowColor: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.15), offSet: CGSize(width: 1.0, height: 1.0), opacity: 1, shadowRadius: 4, cornerRadius: 9.0, corners: [.allCorners])
        
        registerButton.layer.cornerRadius = registerButton.frame.height / 2
        
        registerButton.addTarget(self, action: #selector(registerTap(_:)), for: .touchUpInside)
    }
    
    //MARK: - Set Data
    override func setData() {
        super.setData()
        
    }
    
    //MARK: - Action Handler
    @objc private func registerTap(_ sender: UIButton) {
        self.appDelegate.showNavigation(storyboardName: "Main", controllerName: "HomeNavController", animationOption: .showHideTransitionViews)
    }
    
}
