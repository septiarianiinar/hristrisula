//
//  HomeViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 29/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class HomeViewController: BaseViewController {

    @IBOutlet weak var notifBadgeView: UIView!
    @IBOutlet weak var notifBadgeLabel: UILabel!
    @IBOutlet weak var notifButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileNikLabel: UILabel!
    @IBOutlet weak var profileBackgroundView: UIView!
    
    @IBOutlet weak var totalPengobatanView: UIView!
    @IBOutlet weak var totalSummaryView: UIView!
    
    @IBOutlet var menuButton: [UIButton]!
    
    @IBOutlet weak var transparentBackgroundView: UIView!
    @IBOutlet weak var transparentBackgroundButton: UIButton!
    @IBOutlet weak var pengajuanView: UIView!
    @IBOutlet weak var daftarPengajuanButton: UIButton!
    @IBOutlet weak var riwayatPengajuanButton: UIButton!
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        transparentBackgroundView.isHidden = true
        transparentBackgroundButton.isHidden = true
        pengajuanView.isHidden = true
        
        profileBackgroundView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10.0)
        notifBadgeView.layer.cornerRadius = notifBadgeView.frame.height / 2
        notifBadgeView.isHidden = true
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
        profileImageView.clipsToBounds = true
        
        totalPengobatanView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        totalPengobatanView.layer.borderWidth = 1.0
        totalPengobatanView.layer.cornerRadius = 10.0
        totalSummaryView.layer.cornerRadius = 10.0
        
        //Button Handler
        notifButton.addTarget(self, action: #selector(notifTap(_:)), for: .touchUpInside)
        profileButton.addTarget(self, action: #selector(profileTap(_:)), for: .touchUpInside)
        
        transparentBackgroundButton.addTarget(self, action: #selector(transparentButtonTap(_:)), for: .touchUpInside)
        daftarPengajuanButton.addTarget(self, action: #selector(daftarPengajuanTap(_:)), for: .touchUpInside)
        riwayatPengajuanButton.addTarget(self, action: #selector(riwayatPengajuanTap(_:)), for: .touchUpInside)
    }
    
    //MARK: - Set Data
    override func refreshData() {
        super.refreshData()
        
    }
    
    //MARK: - Action Handler
    @objc private func notifTap(_ sender: UIButton) {
        let pemberitahuanVC = self.storyboard?.instantiateViewController(withIdentifier: "PemberitahuanViewController") as! PemberitahuanViewController
        
        if let viewControllers = self.navigationController?.viewControllers{
            if let index = viewControllers.index(of: self){
                var tmp = viewControllers
                tmp[index] = pemberitahuanVC
                
                self.navigationController?.pushViewController(pemberitahuanVC, animated: true)
            }
        }
    }
    
    @objc private func profileTap(_ sender: UIButton) {
        let ProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoViewController") as! PersonalInfoViewController
        
        if let viewControllers = self.navigationController?.viewControllers{
            if let index = viewControllers.index(of: self){
                var tmp = viewControllers
                tmp[index] = ProfileVC
                
                self.navigationController?.pushViewController(ProfileVC, animated: true)
            }
        }
    }
    
    @IBAction private func pengobatanTap(_ sender: UIButton) {
        let storyboardPengobatan = UIStoryboard(name: "Pengobatan", bundle: nil)
        let pengobatanVC = storyboardPengobatan.instantiateViewController(withIdentifier: "PengobatanViewController") as! PengobatanViewController
        self.navigationController?.pushViewController(pengobatanVC, animated: true)
    }
    
    @IBAction private func absensiTap(_ sender: UIButton) {
        let storyboardAbsensi = UIStoryboard(name: "Absensi", bundle: nil)
        let absensiVC = storyboardAbsensi.instantiateViewController(withIdentifier: "AbsensiViewController") as! AbsensiViewController
        let navigationControllerAbsensi = UINavigationController(rootViewController: absensiVC)
        self.navigationController?.pushViewController(absensiVC, animated: true)
    }
    
    @IBAction private func cutiTap(_ sender: UIButton) {
        let storyboardCuti = UIStoryboard(name: "Cuti", bundle: nil)
        let cutiVC = storyboardCuti.instantiateViewController(withIdentifier: "CutiViewController") as! CutiViewController
        self.navigationController?.pushViewController(cutiVC, animated: true)
    }
    
    @IBAction private func pelatihanTap(_ sender: UIButton) {
        let storyboardPelatihan = UIStoryboard(name: "Pelatihan", bundle: nil)
        let pelatihanVC = storyboardPelatihan.instantiateViewController(withIdentifier: "PelatihanViewController") as! PelatihanViewController
        self.navigationController?.pushViewController(pelatihanVC, animated: true)
    }
    
    @IBAction private func pengajuanTap(_ sender: UIButton) {
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let daftarPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "DaftarPengajuanViewController") as! DaftarPengajuanViewController
        self.navigationController?.pushViewController(daftarPengajuanVC, animated: true)
    }
    
    @IBAction private func menuTap(_ sender: AnyObject) {
        guard let button = sender as? UIButton, let tag = button.tag as? Int else {
            return
        }
        
        switch tag {
        case 1:
//            Toast(text: "Absensi").show()
            let storyboardAbsensi = UIStoryboard(name: "Absensi", bundle: nil)
            let absensiVC = storyboardAbsensi.instantiateViewController(withIdentifier: "AbsensiViewController") as! AbsensiViewController
            let navigationControllerAbsensi = UINavigationController(rootViewController: absensiVC)
            self.navigationController?.pushViewController(absensiVC, animated: true)
//            self.present(navigationControllerAbsensi, animated: true, completion: nil)
            
        case 2:
            let storyboardIzin = UIStoryboard(name: "IzinSakit", bundle: nil)
            let izinSakitVC = storyboardIzin.instantiateViewController(withIdentifier: "IzinSakitViewController") as! IzinSakitViewController
            self.navigationController?.pushViewController(izinSakitVC, animated: true)
            
        case 3:
            let storyboardCuti = UIStoryboard(name: "Cuti", bundle: nil)
            let cutiVC = storyboardCuti.instantiateViewController(withIdentifier: "CutiViewController") as! CutiViewController
            self.navigationController?.pushViewController(cutiVC, animated: true)
            
        case 4:
            let storyboardPengobatan = UIStoryboard(name: "Pengobatan", bundle: nil)
            let pengobatanVC = storyboardPengobatan.instantiateViewController(withIdentifier: "PengobatanViewController") as! PengobatanViewController
            self.navigationController?.pushViewController(pengobatanVC, animated: true)
            
        case 5:
            let storyboardReimburse = UIStoryboard(name: "Reimburse", bundle: nil)
            let reimburseVC = storyboardReimburse.instantiateViewController(withIdentifier: "ReimburseViewController") as! ReimburseViewController
            self.navigationController?.pushViewController(reimburseVC, animated: true)
            
        case 6:
            let storyboardDinas = UIStoryboard(name: "PerjalananDinas", bundle: nil)
            let dinasVC = storyboardDinas.instantiateViewController(withIdentifier: "DinasViewController") as! DinasViewController
            self.navigationController?.pushViewController(dinasVC, animated: true)
            
        case 7:
            let storyboardPelatihan = UIStoryboard(name: "Pelatihan", bundle: nil)
            let pelatihanVC = storyboardPelatihan.instantiateViewController(withIdentifier: "PelatihanViewController") as! PelatihanViewController
            self.navigationController?.pushViewController(pelatihanVC, animated: true)
            
        case 8:
            transparentBackgroundView.isHidden = false
            transparentBackgroundButton.isHidden = false
            pengajuanView.isHidden = false
            
        default:
            break
        }
    }
    
    @objc private func transparentButtonTap(_ sender: UIButton) {
        transparentBackgroundView.isHidden = true
        transparentBackgroundButton.isHidden = true
        pengajuanView.isHidden = true
    }
    
    @objc private func daftarPengajuanTap(_ sender: UIButton) {
        transparentBackgroundView.isHidden = true
        transparentBackgroundButton.isHidden = true
        pengajuanView.isHidden = true
        
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let daftarPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "DaftarPengajuanViewController") as! DaftarPengajuanViewController
        self.navigationController?.pushViewController(daftarPengajuanVC, animated: true)
    }
    
    @objc private func riwayatPengajuanTap(_ sender: UIButton) {
        transparentBackgroundView.isHidden = true
        transparentBackgroundButton.isHidden = true
        pengajuanView.isHidden = true
        
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let riwayatPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "RiwayatPengajuanViewController") as! RiwayatPengajuanViewController
        self.navigationController?.pushViewController(riwayatPengajuanVC, animated: true)
    }

}
