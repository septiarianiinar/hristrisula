//
//  PelatihanViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class PelatihanViewController: BaseViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var riwayatPengajuanButton: UIButton!
    
    @IBOutlet weak var selanjutnyaButton: UIButton!
    @IBOutlet weak var riwayatButton: UIButton!
    @IBOutlet weak var pelatihanTableView: UITableView!
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        
        let backButtonSize:CGFloat = (self.navigationController?.navigationBar.frame.height ?? 0.0) - 5.0
        let backImage:UIImage = self.imageWithImage(image: (UIImage(named: "back_arrow") ?? nil)!, scaledToSize: CGSize(width: (backButtonSize - 10.0), height: (backButtonSize - 10.0)))
        let backBtn = UIButton()
        backBtn.addTarget(self, action: #selector(backTap(_:)), for: .touchUpInside)
        backBtn.setImage(backImage, for: .normal)
        backBtn.contentMode = .scaleAspectFit
        backBtn.frame = CGRect(x: 0, y: 0, width: backButtonSize, height: backButtonSize)
        let backButton = UIBarButtonItem.init()
        backButton.customView = backBtn
        
        self.navigationItem.leftBarButtonItems = [backButton]
        
        let titleLabel = UILabel()
        titleLabel.text = "PELATIHAN SELANJUTNYA"
        titleLabel.font = UIFont(name: "Avenir-Black", size: 20.0)
        titleLabel.textColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1)
        let titleItem = UIBarButtonItem.init()
        titleItem.customView = titleLabel
        
        self.navigationItem.rightBarButtonItems = [titleItem]
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        //Set Category View
        selanjutnyaButton.layer.cornerRadius = 10.0
        selanjutnyaButton.clipsToBounds = true
        riwayatButton.layer.cornerRadius = 10.0
        riwayatButton.clipsToBounds = true
        
        //Set Table View
        pelatihanTableView.separatorStyle = .none
        pelatihanTableView.register(UINib.init(nibName: "pelatihanTableViewCell", bundle: nil), forCellReuseIdentifier: "pelatihanCell")
        
        //Set Button Handler
        selanjutnyaButton.addTarget(self, action: #selector(selanjutnyaTap(_:)), for: .touchUpInside)
        riwayatButton.addTarget(self, action: #selector(riwayatTap(_:)), for: .touchUpInside)
        
        homeButton.addTarget(self, action: #selector(homeBtnTap(_:)), for: .touchUpInside)
        addButton.addTarget(self, action: #selector(addTap(_:)), for: .touchUpInside)
        riwayatPengajuanButton.addTarget(self, action: #selector(riwayatPengajuanTap(_:)), for: .touchUpInside)
    }
    
    //MARK: - Refresh Data
    override func refreshData() {
        super.refreshData()
        
    }
    
    //MARK: - Action Handler
    @objc private func backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func homeBtnTap(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func addTap(_ sender: UIButton) {
        let tambahPelatihanVC = self.storyboard?.instantiateViewController(withIdentifier: "TambahPelatihanViewController") as! TambahPelatihanViewController
        
        if let viewControllers = self.navigationController?.viewControllers{
            if let index = viewControllers.index(of: self){
                var tmp = viewControllers
                tmp[index] = tambahPelatihanVC
                
                self.navigationController?.pushViewController(tambahPelatihanVC, animated: true)
            }
        }
    }
    
    @objc private func riwayatPengajuanTap(_ sender: UIButton) {
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let riwayatPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "RiwayatPengajuanViewController") as! RiwayatPengajuanViewController
        self.navigationController?.pushViewController(riwayatPengajuanVC, animated: true)
    }
    
    @objc private func selanjutnyaTap(_ sender: UIButton) {
        Toast(text: "Soon").show()
    }
    
    @objc private func riwayatTap(_ sender: UIButton) {
        Toast(text: "Soon").show()
    }

}

extension PelatihanViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pelatihanCell", for: indexPath) as! pelatihanTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
