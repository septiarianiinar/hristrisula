//
//  AbsensiViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 30/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class AbsensiViewController: BaseViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var riwayatPengajuanButton: UIButton!
    
    @IBOutlet weak var periodeLabel: UILabel!
    @IBOutlet weak var totalHadirView: UIView!
    @IBOutlet weak var totalHadirLabel: UILabel!
    @IBOutlet weak var totalIzinView: UIView!
    @IBOutlet weak var totalIzinLabel: UILabel!
    @IBOutlet weak var totalCutiView: UIView!
    @IBOutlet weak var totalCutiLabel: UILabel!
    @IBOutlet weak var totalDinasView: UIView!
    @IBOutlet weak var totalDinasLabel: UILabel!
    @IBOutlet weak var totalMangkirView: UIView!
    @IBOutlet weak var totalMangkirLabel: UILabel!
    @IBOutlet weak var absensiTableView: UITableView!
    
    var sideMenuTransition = SlideInTransition()
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        
        let backButtonSize:CGFloat = (self.navigationController?.navigationBar.frame.height ?? 0.0) - 5.0
        let backImage:UIImage = self.imageWithImage(image: (UIImage(named: "back_arrow") ?? nil)!, scaledToSize: CGSize(width: (backButtonSize - 10.0), height: (backButtonSize - 10.0)))
        let backBtn = UIButton()
        backBtn.addTarget(self, action: #selector(backTap(_:)), for: .touchUpInside)
        backBtn.setImage(backImage, for: .normal)
        backBtn.contentMode = .scaleAspectFit
        backBtn.frame = CGRect(x: 0, y: 0, width: backButtonSize, height: backButtonSize)
        let backButton = UIBarButtonItem.init()
        backButton.customView = backBtn
        
        self.navigationItem.leftBarButtonItems = [backButton]
        
        let titleLabel = UILabel()
        titleLabel.text = "ABSENSI"
        titleLabel.font = UIFont(name: "Avenir-Black", size: 20.0)
        titleLabel.textColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1)
        let titleItem = UIBarButtonItem.init()
        titleItem.customView = titleLabel
        
        self.navigationItem.rightBarButtonItems = [titleItem]
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        //Set Summary View
        totalHadirView.layer.borderWidth = 1.0
        totalHadirView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        totalIzinView.layer.borderWidth = 1.0
        totalIzinView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        totalCutiView.layer.borderWidth = 1.0
        totalCutiView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        totalDinasView.layer.borderWidth = 1.0
        totalDinasView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        totalMangkirView.layer.borderWidth = 1.0
        totalMangkirView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        
        //Set Absensi Table View
        absensiTableView.layer.borderWidth = 1.0
        absensiTableView.layer.borderColor = UIColor(red: 235/255, green: 238/255, blue: 247/255, alpha: 1).cgColor
        absensiTableView.separatorStyle = .none
        absensiTableView.register(UINib.init(nibName: "absensiTableViewCell", bundle: nil), forCellReuseIdentifier: "absensiCell")
        
        homeButton.addTarget(self, action: #selector(homeBtnTap(_:)), for: .touchUpInside)
        addButton.addTarget(self, action: #selector(addTap(_:)), for: .touchUpInside)
        riwayatPengajuanButton.addTarget(self, action: #selector(riwayatPengajuanTap(_:)), for: .touchUpInside)
    }
    
    //MARK: - Set Data
    override func refreshData() {
        super.refreshData()
        
    }
    
    //MARK: - Action Handler
    @objc private func backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func homeBtnTap(_ sender: UIButton) {
//        self.appDelegate.showNavigation(storyboardName: "Main", controllerName: "HomeNavController", animationOption: .transitionCrossDissolve)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func addTap(_ sender: UIButton) {
        let tambahAbsensiVC = self.storyboard?.instantiateViewController(withIdentifier: "TambahAbsensiViewController") as! TambahAbsensiViewController
        
        if let viewControllers = self.navigationController?.viewControllers{
            if let index = viewControllers.index(of: self){
                var tmp = viewControllers
                tmp[index] = tambahAbsensiVC
                
//                self.navigationController?.present(tambahAbsensiVC, animated: true, completion: nil)
                self.navigationController?.pushViewController(tambahAbsensiVC, animated: true)
            }
        }
    }
    
    @objc private func riwayatPengajuanTap(_ sender: UIButton) {
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let riwayatPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "RiwayatPengajuanViewController") as! RiwayatPengajuanViewController
        self.navigationController?.pushViewController(riwayatPengajuanVC, animated: true)
    }

}

extension AbsensiViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "absensiCell", for: indexPath) as! absensiTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//Slide Transition Delegate
extension AbsensiViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        sideMenuTransition.isPresenting = true
        return sideMenuTransition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        sideMenuTransition.isPresenting = false
        return sideMenuTransition
    }
}
