//
//  SplashscreenGradientView.swift
//  HRISTrisula
//
//  Created by MacBook on 05/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

@IBDesignable
class SplashscreenGradientView: UIView {

    @IBInspectable var color1: UIColor = UIColor.clear {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var color2: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var color3: UIColor = UIColor.clear {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var color4: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var color5: UIColor = UIColor.clear {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var color6: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [color1, color2, color3, color4, color5, color6].map{$0.cgColor}
        layer.cornerRadius = 5.0
    }

}
