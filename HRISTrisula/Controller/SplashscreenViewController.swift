//
//  SplashscreenViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 29/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class SplashscreenViewController: BaseViewController {
    
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.appDelegate.showNavigation(storyboardName: "Main", controllerName: "RegistrasiViewController", animationOption: .showHideTransitionViews)
        }
    }
    
}
