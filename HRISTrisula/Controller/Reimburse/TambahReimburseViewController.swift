//
//  TambahReimburseViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class TambahReimburseViewController: BaseViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var simpanButton: UIButton!
    @IBOutlet weak var riwayatPengajuanButton: UIButton!
    
    @IBOutlet weak var tipeView: UIView!
    @IBOutlet weak var tipeLabel: UILabel!
    @IBOutlet weak var tipeButton: UIButton!
    @IBOutlet weak var tipeTableView: UITableView!
    @IBOutlet weak var tanggalKuitansiView: UIView!
    @IBOutlet weak var tanggalKuitansiField: UITextField!
    @IBOutlet weak var nominalView: UIView!
    @IBOutlet weak var nominalField: UITextField!
    @IBOutlet weak var keteranganView: UIView!
    @IBOutlet weak var keteranganTextView: UITextView!
    @IBOutlet weak var keteranganTextViewHeight: NSLayoutConstraint!
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        
        let backButtonSize:CGFloat = (self.navigationController?.navigationBar.frame.height ?? 0.0) - 5.0
        let backImage:UIImage = self.imageWithImage(image: (UIImage(named: "back_arrow") ?? nil)!, scaledToSize: CGSize(width: (backButtonSize - 10.0), height: (backButtonSize - 10.0)))
        let backBtn = UIButton()
        backBtn.addTarget(self, action: #selector(backTap(_:)), for: .touchUpInside)
        backBtn.setImage(backImage, for: .normal)
        backBtn.contentMode = .scaleAspectFit
        backBtn.frame = CGRect(x: 0, y: 0, width: backButtonSize, height: backButtonSize)
        let backButton = UIBarButtonItem.init()
        backButton.customView = backBtn
        
        self.navigationItem.leftBarButtonItems = [backButton]
        
        let titleLabel = UILabel()
        titleLabel.text = "REIMBURSE"
        titleLabel.font = UIFont(name: "Avenir-Black", size: 20.0)
        titleLabel.textColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1)
        let titleItem = UIBarButtonItem.init()
        titleItem.customView = titleLabel
        
        self.navigationItem.rightBarButtonItems = [titleItem]
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        //Set Layout Form
        tipeView.layer.borderWidth = 1.0
        tipeView.layer.borderColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1).cgColor
        tanggalKuitansiView.layer.borderWidth = 1.0
        tanggalKuitansiView.layer.borderColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1).cgColor
        nominalView.layer.borderWidth = 1.0
        nominalView.layer.borderColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1).cgColor
        keteranganView.layer.borderWidth = 1.0
        keteranganView.layer.borderColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1).cgColor
        
        simpanButton.layer.cornerRadius = 10.0
        
        //Set Button Handler
        homeButton.addTarget(self, action: #selector(homeBtnTap(_:)), for: .touchUpInside)
        simpanButton.addTarget(self, action: #selector(simpanTap(_:)), for: .touchUpInside)
        riwayatPengajuanButton.addTarget(self, action: #selector(riwayatPengajuanTap(_:)), for: .touchUpInside)
    }
    
    //MARK: - Set Data
    override func setData() {
        super.setData()
        
    }
    
    //MARK: - Action Handler
    @objc private func backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func homeBtnTap(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func simpanTap(_ sender: UIButton) {
        Toast(text: "Soon").show()
    }
    
    @objc private func riwayatPengajuanTap(_ sender: UIButton) {
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let riwayatPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "RiwayatPengajuanViewController") as! RiwayatPengajuanViewController
        self.navigationController?.pushViewController(riwayatPengajuanVC, animated: true)
    }

}
