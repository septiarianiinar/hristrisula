//
//  BaseViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 29/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import UserNotifications
import AlamofireImage

class BaseViewController: UIViewController, ValidationDelegate {

    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
    let validator = Validator()
    var refresher:UIRefreshControl = {
        let tmpRefresher = UIRefreshControl()
        tmpRefresher.attributedTitle = NSAttributedString(
            string: "Loading Data",
            attributes: [
                //                NSAttributedStringKey.font :UIFont(name: "Ubuntu", size: 11.0)!,
                NSAttributedString.Key.foregroundColor:UIColor.white
            ])
        tmpRefresher.tintColor = UIColor.white
        tmpRefresher.backgroundColor = UIColor(red: 60.0/255, green: 157.0/255, blue: 59.0/255, alpha: 1)
        return tmpRefresher
    }()
    
    var performingSegueId = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setView()
        setData()
        setValidation()
    }
    
    func setView(){}
    
    func setData(){}
    
    func setValidation(){
        setValidator()
        registerValidationField()
    }
    
    func setValidator() {
        validator.styleTransformers(
            success:{ (validationRule) -> Void in
                validationRule.errorLabel?.isHidden = true
                validationRule.errorLabel?.text = ""
        },
            error:{ (validationError) -> Void in
                validationError.errorLabel?.isHidden = false
                validationError.errorLabel?.text = validationError.errorMessage
        }
        )
    }
    
    func registerValidationField() {}
    
    func validationSuccessful() {}
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        refreshView()
        setFirstResponder()
    }
    
    func refreshView() {}
    
    func setFirstResponder() {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshData()
        setNavigationBar()
        addObeservers()
    }
    
    func refreshData() {}
    
    func setNavigationBar() {}
    
    func addObeservers() {}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        removeObservers()
        self.performingSegueId = []
        self.backToPreviousWithData()
    }
    
    func backToPreviousWithData() {}
    
    func removeObservers(){
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.view.backgroundColor = UIColor(red: 17/255, green: 35/255, blue: 70/255, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 17/255, green: 35/255, blue: 70/255, alpha: 1)
        UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        
        //        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //        UINavigationBar.appearance().tintColor = UIColor.white
        //        UINavigationBar.appearance().backgroundColor =  UIColor(red: 17/255, green: 35/255, blue: 70/255, alpha: 1)
    }
    
    //MARK: - trigger auth
    func showAuth(){
        guard let authNav = self.storyboard?.instantiateViewController(withIdentifier: "auth") else{return}
        self.present(authNav, animated: true, completion: nil)
    }
    
    //MARK: - Navigation
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if performingSegueId.contains(identifier){
            return
        }
        
        performingSegueId.append(identifier)
        super.performSegue(withIdentifier: identifier, sender: sender)
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }

}

extension BaseViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    // DROP SHADOW 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // DROP SHADOW 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    //ADD SHADOW 3
    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .white) {
        
        let shadowLayer = CAShapeLayer()
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath //1
        shadowLayer.path = cgPath //2
        shadowLayer.fillColor = fillColor.cgColor //3
        shadowLayer.shadowColor = shadowColor.cgColor //4
        shadowLayer.shadowPath = cgPath
        shadowLayer.shadowOffset = offSet //5
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius
        self.layer.addSublayer(shadowLayer)
    }
}

//MARK: - UISearchBar EXTENSION
extension UISearchBar {
    
    private func getViewElement<T>(type: T.Type) -> T? {
        
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    func getSearchBarTextField() -> UITextField? {
        return getViewElement(type: UITextField.self)
    }
    
    func setTextColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            textField.textColor = color
        }
    }
    
    func setTextFontSize(size: CGFloat) {
        
        if let textField = getSearchBarTextField() {
            textField.font = UIFont.systemFont(ofSize: size)
        }
    }
    
    func setTextFieldColor(color: UIColor) {
        
        if let textField = getViewElement(type: UITextField.self) {
            switch searchBarStyle {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 6
            case .prominent, .default:
                textField.backgroundColor = color
            }
        }
    }
    
    func setPlaceholderTextColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            textField.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor: color])
        }
    }
    
    func setTextFieldClearButtonColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            
            let button = textField.value(forKey: "clearButton") as! UIButton
            if let image = button.imageView?.image {
                button.setImage(image.transform(withNewColor: color), for: .normal)
            }
        }
    }
    
    func setSearchImageColor(color: UIColor) {
        
        if let imageView = getSearchBarTextField()?.leftView as? UIImageView {
            imageView.image = imageView.image?.transform(withNewColor: color)
        }
    }
}

extension UIImage {
    
    func transform(withNewColor color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
