//
//  TambahPengobatanViewController.swift
//  HRISTrisula
//
//  Created by MacBook on 04/12/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster

class TambahPengobatanViewController: BaseViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var simpanButton: UIButton!
    @IBOutlet weak var riwayatPengajuanButton: UIButton!
    
    @IBOutlet weak var namaKuitansiView: UIView!
    @IBOutlet weak var namaKuitansiLabel: UILabel!
    @IBOutlet weak var namaKuitansiButton: UIButton!
    @IBOutlet weak var namaKuitansiTableView: UITableView!
    @IBOutlet weak var tanggalKuitansiView: UIView!
    @IBOutlet weak var tanggalKuitansiField: UITextField!
    @IBOutlet weak var diagnosaView: UIView!
    @IBOutlet weak var diagnosaTextView: UITextView!
    @IBOutlet weak var diagnosaTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nominalView: UIView!
    @IBOutlet weak var nominalField: UITextField!
    @IBOutlet weak var nilaiGantiView: UIView!
    @IBOutlet weak var nilaiGantiField: UITextField!
    @IBOutlet weak var sisaPlafonView: UIView!
    @IBOutlet weak var sisaPlafonField: UITextField!
    
    var tipeMedical:String?
    
    //MARK: - Set Navigation Bar
    override func setNavigationBar() {
        super.setNavigationBar()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
        
        let backButtonSize:CGFloat = (self.navigationController?.navigationBar.frame.height ?? 0.0) - 5.0
        let backImage:UIImage = self.imageWithImage(image: (UIImage(named: "back_arrow") ?? nil)!, scaledToSize: CGSize(width: (backButtonSize - 10.0), height: (backButtonSize - 10.0)))
        let backBtn = UIButton()
        backBtn.addTarget(self, action: #selector(backTap(_:)), for: .touchUpInside)
        backBtn.setImage(backImage, for: .normal)
        backBtn.contentMode = .scaleAspectFit
        backBtn.frame = CGRect(x: 0, y: 0, width: backButtonSize, height: backButtonSize)
        let backButton = UIBarButtonItem.init()
        backButton.customView = backBtn
        
        self.navigationItem.leftBarButtonItems = [backButton]
        
        let titleLabel = UILabel()
        if let tipe = tipeMedical as? String {
            titleLabel.text = tipe.uppercased()
        } else {
            titleLabel.text = "PENGOBATAN"
        }
        titleLabel.font = UIFont(name: "Avenir-Black", size: 20.0)
        titleLabel.textColor = UIColor(red: 13/255, green: 46/255, blue: 94/255, alpha: 1)
        let titleItem = UIBarButtonItem.init()
        titleItem.customView = titleLabel
        
        self.navigationItem.rightBarButtonItems = [titleItem]
    }
    
    //MARK: - Set View
    override func setView() {
        super.setView()
        
        //Set Layout Form
        namaKuitansiView.layer.borderWidth = 1.0
        namaKuitansiView.layer.borderColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1).cgColor
        tanggalKuitansiView.layer.borderWidth = 1.0
        tanggalKuitansiView.layer.borderColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1).cgColor
        diagnosaView.layer.borderWidth = 1.0
        diagnosaView.layer.borderColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1).cgColor
        nominalView.layer.borderWidth = 1.0
        nominalView.layer.borderColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1).cgColor
        nilaiGantiView.layer.borderWidth = 1.0
        nilaiGantiView.layer.borderColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1).cgColor
        sisaPlafonView.layer.borderWidth = 1.0
        sisaPlafonView.layer.borderColor = UIColor(red: 93/255, green: 156/255, blue: 236/255, alpha: 1).cgColor
        
        simpanButton.layer.cornerRadius = 10.0
        
        //Set Button Handler
        homeButton.addTarget(self, action: #selector(homeBtnTap(_:)), for: .touchUpInside)
        simpanButton.addTarget(self, action: #selector(simpanTap(_:)), for: .touchUpInside)
        riwayatPengajuanButton.addTarget(self, action: #selector(riwayatPengajuanTap(_:)), for: .touchUpInside)
    }
    
    //MARK: - Set Data
    override func setData() {
        super.setData()
        
    }
    
    //MARK: - Action Handler
    @objc private func backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func homeBtnTap(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func simpanTap(_ sender: UIButton) {
        Toast(text: "Soon").show()
    }
    
    @objc private func riwayatPengajuanTap(_ sender: UIButton) {
        let storyboardPengajuan = UIStoryboard(name: "Pengajuan", bundle: nil)
        let riwayatPengajuanVC = storyboardPengajuan.instantiateViewController(withIdentifier: "RiwayatPengajuanViewController") as! RiwayatPengajuanViewController
        self.navigationController?.pushViewController(riwayatPengajuanVC, animated: true)
    }

}
